﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trinity;
using UndirectedGraph;

namespace MyApp
{
    class MyServer : MyServerBase
    {
        internal static int GetServerIdByKey(string key)
        {
            return 0;
        }
        public override void GetHandler(GetMessageReader request, GetResponseWriter response)
        {
            //Console.WriteLine("Getting");
            int node1 = request.node1;
            int node2 = request.node2;
            int p = 0;
            int q = 1;
            List<int> queue = new List<int>();
            queue.Add(node1);
            int N = Global.LocalStorage.LoadGraphMeta(-1).N;
            //Console.WriteLine("The Number of Node is " + Convert.ToInt32(N));
            var visited = new bool[N];
            var best = new int[N];
            for (int i = 0; i < N; i++)
            {
                visited[i] = false;
                best[i] = -1;
            }
            best[node1] = 0;
            visited[node1] = true;

            while (p < q)
            {
                int x = queue[p];
                //Console.WriteLine("NOD X:");
                //Console.WriteLine(x);
                p++;
                var edgeList = Global.LocalStorage.LoadGraphNode(x).edges;
                foreach (var y in edgeList) {
                    //Console.WriteLine("NEB Y:");
                    //Console.WriteLine(y);
                    if (!visited[y])
                    {
                        visited[y] = true;
                        best[y] = best[x] + 1;
                        queue.Add(y);
                        q++;
                    }
                }
            }

            response.distance = best[node2];
            if (visited[node2] == false)
            {
                response.isConnected = false;
            }
            else
            {
                response.isConnected = true;
            }
        }

        public override void SetHandler(SetMessageReader request, SetResponseWriter response)
        {
            //Console.WriteLine("Setting");
            var node = Global.LocalStorage.LoadGraphNode(request.node1);
            node.edges.Add(request.node2);
            Global.LocalStorage.SaveGraphNode(node);
            response.set = true;
        }
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            TrinityConfig.AddServer(new Trinity.Network.ServerInfo("127.0.0.1", 5304, Global.MyAssemblyPath, Trinity.Diagnostics.LogLevel.Error)); 
            //TrinityConfig.AddServer(new Trinity.Network.ServerInfo("10.171.6.133", 5304, Global.MyAssemblyPath, Trinity.Diagnostics.LogLevel.Error)); 
            if (args.Length < 1)
            {
                Console.WriteLine("Please provide a command line parameter (-s|-c).");
                Console.WriteLine("  -s  Start as a distributed hashtable server.");
                Console.WriteLine("  -c  Start as a distributed hashtable client.");
                return;
            }

            if (args[0].Trim().ToLower().StartsWith("-s"))
            {
                Console.WriteLine("Welcome to the GE Demo!");
                Console.WriteLine("Server Starting");
                MyServer server = new MyServer();
                Console.WriteLine("Graph Generating");
                int NodeNum = 1000;
                if (args.Length >= 2)
                {
                    NodeNum = Convert.ToInt32(args[1]);
                }
                GenerateGraph(NodeNum);
                Console.WriteLine("Graph Generated");    
                server.Start();
                Console.WriteLine("Server Started");
            }

            if (args[0].Trim().ToLower().StartsWith("-c"))
            {
                if (args.Length >= 2) 
                {
                    BatchQueryTest(Convert.ToInt32(args[1]));
                }
                else 
                {
                    HandlingUserInput();
                }
            }
        }

        static void GenerateGraph(int NodeNum)
        {
            var meta = new GraphMeta();
            meta.N = NodeNum;
            meta.CellID = -1;
            meta.d = 3;
            Global.LocalStorage.SaveGraphMeta(meta);

            for (int i = 0; i < meta.N; i++)
            {
                var node = new GraphNode();
                node.CellID = i;
                node.name = Convert.ToString(i);
                node.edges = new List<int>();


                for (int j = -meta.d; j <= meta.d; j++)
                {
                    if (i + j < 0) continue;
                    if (i + j >= meta.N) continue;
                    node.edges.Add(i + j);
                }
                    
                Global.LocalStorage.SaveGraphNode(node);
                //Console.WriteLine("node is created");
            }
        }

        static void HandlingUserInput()
        {
            TrinityConfig.CurrentRunningMode = RunningMode.Client;
            while (true)
            {
                Console.WriteLine("Please input a command (set|get):");
                string input = Console.ReadLine().Trim();
                string[] fields = input.Split(new char[] { '(', ',', ')' }, StringSplitOptions.RemoveEmptyEntries);

                if (fields.Length > 0)
                {
                    string command = fields[0].Trim().ToLower();

                    #region get
                    if (command.Equals("get"))
                    {
                        if (fields.Length < 3)
                        {
                            Console.WriteLine("example: get(key1, value1) or get(\"key 2\", \"value 2\")");
                            continue;
                        }
                        string key = fields[1].Trim().Trim(new char[] { '\"' });
                        string value = fields[2].Trim().Trim(new char[] { '\"' });
                        using (var request = new GetMessageWriter(Convert.ToInt32(key), Convert.ToInt32(value)))
                        {
                            using (var response = Global.CloudStorage.GetToMyServer(MyServer.GetServerIdByKey(key), request))
                            {
                                if (response.isConnected)
                                    Console.WriteLine("Distance is " + Convert.ToString(response.distance));
                                else
                                    Console.WriteLine("Not Connected.");
                            }
                        }
                        continue;
                    }
                    #endregion

                    #region set
                    if (command.Equals("set"))
                    {
                        if (fields.Length < 3)
                        {
                            Console.WriteLine("example: set(key1, value1) or set(\"key 2\", \"value 2\")");
                            continue;
                        }
                        string key = fields[1].Trim().Trim(new char[] { '\"' });
                        string value = fields[2].Trim().Trim(new char[] { '\"' });
                        using (var request = new SetMessageWriter(Convert.ToInt32(key), Convert.ToInt32(value)))
                        {
                            Console.WriteLine("Set the value of \"{0}\" is \"{1}\"", key, value);
                            using (var response = Global.CloudStorage.SetToMyServer(MyServer.GetServerIdByKey(key), request))
                            {
                                if (response.set)
                                    Console.WriteLine("Successfully Set");
                                else
                                    Console.WriteLine("Failure");
                            }
                        }
                        continue;
                    }
                    #endregion
                }
                Console.WriteLine("Please input a valid command (set|get):");
            }
        }

        static void BatchQueryTest(int batchSize)
        {
            TrinityConfig.CurrentRunningMode = RunningMode.Client;
            Stopwatch watch = new Stopwatch();//实例化类
            watch.Start();
            
            //int N = Global.LocalStorage.LoadGraphMeta(-1).N;
            int ret = 0;
            //
            for (int i = 0; i < batchSize; i++)
            {
                using (var request = new GetMessageWriter(Convert.ToInt32(0), Convert.ToInt32(1)))
                {
                    using (var response = Global.CloudStorage.GetToMyServer(MyServer.GetServerIdByKey("Foo"), request))
                    {
                        if (response.isConnected)
                            ret++;
                        else
                            ret--;
                    }
                }
            }
            //
            watch.Stop();
            Console.WriteLine("Queries" + Convert.ToInt32(ret));
            Console.WriteLine("Time Elapsed: " + watch.ElapsedMilliseconds.ToString());
        }
    }
}
